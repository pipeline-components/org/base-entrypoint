# What should work

## Commands

1. docker run --rm -ti -v $(pwd):/code entrypoint-test foobar arguments
1. docker run --rm -ti -v $(pwd):/code entrypoint-test arguments
1. gitlab-runner exec docker  --docker-pull-policy never foo

## Special

`docker run --rm -ti -v $(pwd):/code entrypoint-test`
gives the default command
